# Toplevel website for openpgp.dev

Local build:

```
$ podman build .
[..]
--> ef7126429355
ef7126429355cc20bc9d9962944125718e1987148d69825d7af7b1fcf017c6ec
$ podman run -p 127.0.0.1:8090:80 ef7126429355
[..]
```

Preview on <http://localhost:8090/>
